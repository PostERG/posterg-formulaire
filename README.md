# PostERG - Formulaire d'ajout de mémoires

Le formulaire a pour objectif de rendre possible au étudiant.e.s sortant de l'erg en cursus de Master, de mettre à disposition leur mémoires et travaux de fin d'études, dans le cadre du projet PostERG. 

Les métadonnées sont sauvegarder en format .yaml, et toutes les données sont téléversé sur un serveur interne à l'école, pour ensuite être afficher publiquement en ligne à travers [le siteweb PostERG](https://codeberg.org/PostERG/posterg-website). 

Le formulaire est en PHP, utilisant à la fois du css fait-main et [Simple.css](https://simplecss.org/). La librairie de traitement du YAML est [Symfony](https://symfony.com/doc/current/components/yaml.html). 

Pour installer toutes les dépendances citées dans `compose.json`, installer compose sur votre système: 

```shell
curl -sS https://getcomposer.org/installer | php

```
ou 
```shell
php -r "readfile('https://getcomposer.org/installer');" | php

```
ou installer le paquet composer de votre distribution. 

Puis naviguer jusqu'au dossier du projet dans un terminal et lancer la commande: 

```shell
composer install

```

Il vous faudras un serveur php pour voir le site en local. 

Pour cela, utiliser soit WAMP/MAMP/LAMP, soit installé php pour votre système et lancé dans le dossier du projet depuis un terminal la commande suivant: 

```shell
php -S 127.0.0.1:3000

```
Ouvrer votre navigateur de choix (Firefox ;-) ) et entrer l'addresse: 

```
127.0.0.1:3000
``` 
dans la barre d'URL. 
