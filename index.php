
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formulaire</title>
    <link rel="stylesheet" href="assets/normalize.css">
    <link rel="stylesheet" href="assets/simple.css">
    <link rel="stylesheet" href="assets/posterg.css">
    <link rel="shortcut icon" href="assets/icon.svg" type="image/svg">
</head>
<body>
<header>

<h1>Formulaire Posterg</h1>
</header>

<main>
<form action="formulaire.php" method="post" enctype="multipart/form-data">
                <label>Nom/Prénom/Pseudo</label>
                <input type="text" name="auteurice" placeholder="..." required>

                <label>Année diplômante (2023, 2024, ...)</label>
                <input type="text" name="année" placeholder="..." required>

                <label>Orientation principale</label>
                <select name="orientation" required>
                    <option value="">-- Ton orientation --</option>
                    <option value="typographie">Typographie</option>
                    <option value="graphisme">Graphisme</option>
                    <option value="designnumérique">Design Numérique</option>
                    <option value="Cinéma d'animation">Cinéma d'animation</option>
                    <option value="Illustration">Illustration</option>
                    <option value="BD">Bande dessinée</option>
                    <option value="Photographie">Photographie</option>
                    <option value="Vidéographie">Vidéographie</option>
                    <option value="Sculpture">Sculpture</option>
                    <option value="Peinture">Peinture</option>
                    <option value="Art numérique">Art numérique</option>
                    <option value="Vidéographie">Vidéographie</option>
                    <option value="Photographie">Photographie</option>
                    <option value="Dessin">Dessin</option>
                    <option value="Installation performance">Installation performance</option>
                </select>

                <label>Atelier Pratique</label>
                <select name="ap" required>
                    <option value="">-- Ton AP --</option>
                    <option value="DPM">Design et politique du multiple</option>
                    <option value="APS">Art et pratique situé</option>
                    <option value="R&E">Récits et expérimentation</option>
                    <option value="PAOC">Pratique de l'art et outils critiques</option>
                </select>

                <label>Contact : mail, insta, ...</label>
                <input type="email" name="mail" placeholder="Votre contact">

                <label>Titre du mémoire</label>
                <input type="titre" name="titre" placeholder="..." required>

                <label>Tag/mots-clefs sur le mémoire</label>
                <input type="text" name="tag" placeholder="typographie, photographie, outils libre, post-colonial,..">

                <label>Promoteur.rice</label>
                <input type="text" name="promoteurice" placeholder="nom/prénom/pseudo">

                <label>Problématique</label>
                <input type="text" name="problématique" placeholder="Problématique de ton mémoire...">

                <label>Résumé en quelque lignes</label>
                <textarea id="textareaField" rows="8" type="text" name="description" placeholder="Description de ton mémoire..."></textarea>

                <label>Lien vers un site web ou quelque chose d'autres en lignes</label>
                <input type="text" name="lien" placeholder="https://monmémoire.erg.be/...">

                <label>Importer une couverture</label>
                <i>Vérifie que ton fichier est bien un jpg.</i>
                <br>
                <!-- THE FILES[] IS NECESSARY IF THERE ARE MULTIPLE FILES UPLOADED -->
                <input type="file" name="couverture">

                <label>Importer les divers fichers de son mémoire</label>
                <i>Si tu veux importer un dossier, créer une archive zip.</i>
                <!-- THE FILES[] IS NECESSARY IF THERE ARE MULTIPLE FILES UPLOADED -->
                <input type="file" name="files[]" multiple>
                <br>
                <input type="submit" name="go" value="envoyer">
        </form>
</main>

<footer>
    <p>Formulaire fait avec ❤ en PHP et <a href="https://github.com/kevquirk/simple.css">SimpleCSS</a>.</p>
  </footer>
        </body>
</html>