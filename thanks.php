<?php
// Configure error reporting
ini_set('display_errors', 0);
ini_set('log_errors', 1);
ini_set('error_log', 'error.log');

require 'vendor/autoload.php';

use Symfony\Component\Yaml\Yaml;

$yamlFile = isset($_GET['file']) ? urldecode($_GET['file']) : '';
$data = Yaml::parseFile($yamlFile);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ThankYou</title>
    <link rel="stylesheet" href="assets/normalize.css">
    <link rel="stylesheet" href="assets/simple.css">
    <link rel="stylesheet" href="assets/posterg.css">
    <link rel="shortcut icon" href="assets/icon.svg" type="image/svg">

</head>
<body>
    <header>
    <h1>Merci 💜</h1> 
</header>
<main>
<p>d'avoir rempli le formulaire. Le contenu soumis a été sauvegardé et est en attente de traitement.</p>

    <h4>Voici les informations que vous avez encodées dans le formulaire, affiché tel que c'est stocké, en yaml:</h4>
    <pre><code><?php echo htmlspecialchars(Yaml::dump($data)); ?></code></pre>
    <p>Pour revenir au <a href="index.php">formulaire</a>.</p>
</main>
<footer>
    <p>Formulaire fait avec ❤ en PHP et <a href="https://github.com/kevquirk/simple.css">SimpleCSS</a>.</p>
  </footer>
</body>
</html>